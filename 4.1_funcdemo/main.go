package main

import (
  "fmt"
  // "math"
)

func main() {
  foo()
}

// a simple function
func foo() {
  fmt.Println("foo() was called")
}
